# Binar Academy - Chapter 1

What I learn in Binar Academy Backend JavaScript course Chappter 1:

### Table of Contents

1. [15 Feb](#15-feb)
2. [16 Feb](#16-feb)
3. [17 Feb](#17-feb)
4. [18 Feb](#18-feb)
5. [21 Feb](#21-feb)
6. [22 Feb](#22-feb)
7. [23 Feb](#23-feb)

### 15 Feb

Topic learn on this day:

1. Variables
2. Data Types
3. Function
4. Template String
5. Object
6. Array

### 16 Feb

Topic learn on this day:

1. ES6
2. Arrow Function
3. Currying Function
4. Callback
5. Class

### 17 Feb

Topic learn on this day:

1. Visual Studio Code
2. Promise
3. Async Await
4. NPM (package, script)

### 18 Feb

Topic learn on this day:

1. Fetch API using Axios
2. Git
3. Local & Remote Repository
4. Git Command (init, add, commit, remote, push, pull)
5. GitHub & GitLab

### 21 Feb

Topic learn on this day:

1. Git Repository and Workflow
2. Git Branch (main, dev, feature)

### 22 Feb

Topic learn on this day:

1. Do challenge for chapter 1

### 23 Feb

Topic learn on this day:

1.  -
