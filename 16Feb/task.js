class Mobil {
    static bergerakdidarat = true;

    constructor(merek, warna, manual) {
        this.merek = merek;
        this.warna = warna;
        this.manual = manual;
    }

    toString() {
        this.getMerek();
        this.getWarna();
        this.getManual();
    }

    getMerek() {
        console.log(`Merek: ${this.merek}`);
    }

    getWarna() {
        console.log(`Warna: ${this.warna}`);
    }

    getManual() {
        console.log(`Manual: ${this.manual}`);
    }
}

const honda = new Mobil('Honda', 'Putih', true);
honda.getInfoMobil();
console.log(`Bergerak di darat: ${Mobil.bergerakdidarat}`);