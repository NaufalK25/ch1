const axios = require('axios');

const getHeroes = async () => {
    try {
        const result = await axios.get('https://indonesia-public-static-api.vercel.app/api/heroes')
        result.data.forEach(hero => console.log(hero.name));
    } catch (err) {
        console.error(err.message);
    }
}

getHeroes();