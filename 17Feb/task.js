let persediaan = 3;

console.log(`Persediaan awal: ${persediaan}`);

const getPersediaan = (jumlahYangDiambil) => {
    return new Promise((resolve, reject) => {
        if (jumlahYangDiambil <= persediaan) {
            persediaan -= jumlahYangDiambil;
            resolve(`Sisa persediaan: ${persediaan}`);
        } else {
            reject(`Persediaan kurang ${jumlahYangDiambil - persediaan}!`);
        }
    });
}

const get = async (jumlahYangDiambil) => {
    try {
        const result = await getPersediaan(jumlahYangDiambil);
        console.log('Mengambil persediaan...');
        console.log(result);
    } catch (err) {
        console.log('Mengambil persediaan...');
        console.error(err);
    }
}

get(1);
get(2);
get(3);