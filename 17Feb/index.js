const fs = require('fs');

const option = { encoding: "utf-8" };

const callback = (err, data) => {
    console.log('muncul pertama');
    (err) && console.error(err.message);
    console.log(data);
}

const readFile = new Promise((resolve, reject) => {
    fs.readFile('./contoh.txt', option, (err, data) => {
        err ? reject(err) : resolve(data);
    });
});

readFile
    .then(data => {
        console.log('muncul kedua');
        console.log(data);
    })
    .catch(err => {
        console.error(err.message);
    });

// console.log(fs.readFileSync('./contoh.txt', option));

console.log('muncul kedua');