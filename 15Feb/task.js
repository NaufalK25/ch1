let angka1 = 24,
    angka2 = 2;

const tambah = (a, b) => {
    const hasil = a + b;
    return `${a} + ${b} = ${hasil}`;
}
const kurang = (a, b) => {
    const hasil = a - b;
    return `${a} - ${b} = ${hasil}`;
}
const kali = (a, b) => {
    const hasil = a * b;
    return `${a} * ${b} = ${hasil}`;
}
const bagi = (a, b) => {
    const hasil = a / b;
    return `${a} / ${b} = ${hasil}`;
}

console.log(tambah(angka1, angka2));
console.log(kurang(angka1, angka2));
console.log(kali(angka1, angka2));
console.log(bagi(angka1, angka2));
